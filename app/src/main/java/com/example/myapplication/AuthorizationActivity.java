package com.example.myapplication;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

public class AuthorizationActivity extends AppCompatActivity {
    EditText login,password;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_authorization);
        login = findViewById(R.id.loginEditText);
        password = findViewById(R.id.passwordEditText);
    }
    public void loginClick(View view) {
        String myLogin = login.getText().toString();
        String myPassword = password.getText().toString();
        if(!myLogin.equals("") && !myPassword.equals("")){
            if(myLogin.equals("admin") && myPassword.equals("admin")){
                Intent intent = new Intent(AuthorizationActivity.this,NewMenuActivity.class);
                startActivity(intent);
            }
            else {
                Toast.makeText(this,"Логин или пароль неверны",Toast.LENGTH_LONG).show();
            }
        }
        else {
            Toast.makeText(this,"Поля не должны быть пустыми",Toast.LENGTH_LONG).show();
        }

    }

    public void regClick(View view) {
        Intent intent = new Intent(AuthorizationActivity.this,RegistrationActivity.class);
        startActivity(intent);
    }
}