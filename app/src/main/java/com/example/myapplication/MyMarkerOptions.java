package com.example.myapplication;

public class MyMarkerOptions {
    private String snippet;
    private String title;
    private String drawableName;
    private Double latitude;
    private Double longitude;

    public MyMarkerOptions(String snippet, String title, String drawableName, Double latitude, Double longitude) {
        this.snippet = snippet;
        this.title = title;
        this.drawableName = drawableName;
        this.latitude = latitude;
        this.longitude = longitude;
    }
    public String getSnippet() { return snippet; }

    public void setSnippet(String snippet) { this.snippet = snippet; }

    public String getTitle() { return title; }

    public void setTitle(String title) { this.title = title; }

    public String getDrawableName() {
        return drawableName;
    }

    public void setDrawableName(String drawableName) {
        this.drawableName = drawableName;
    }

    public Double getLatitude() {
        return latitude;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    public Double getLongitude() {
        return longitude;
    }

    public void setLongitude(Double longitude) { this.longitude = longitude; }

}
