package com.example.myapplication.ui.menu;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.RecyclerView;

import com.example.myapplication.MenuActivity;
import com.example.myapplication.MyRecyclerViewAdapter;
import com.example.myapplication.ProfileActivity;
import com.example.myapplication.R;

public class MenuFragment extends Fragment {
    RecyclerView recyclerView;
    ImageView profile;
    String[] titles,descriptions;
    int[] images = {R.drawable.pizza,R.drawable.cheezy,R.drawable.double_peperoni,
            R.drawable.four_season,R.drawable.hawaii,R.drawable.margarita,R.drawable.meat,
            R.drawable.mega_pizza,R.drawable.student_pizza,R.drawable.super_sweet};

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {

        View root = inflater.inflate(R.layout.activity_menu, container, false);
        recyclerView = root.findViewById(R.id.pizzaList);
        titles = getResources().getStringArray(R.array.titlePizza);
        descriptions = getResources().getStringArray(R.array.descriptionPizza);
        MyRecyclerViewAdapter recyclerViewAdapter = new MyRecyclerViewAdapter(titles,descriptions,images,getActivity());
        recyclerView.setAdapter(recyclerViewAdapter);

        profile = root.findViewById(R.id.userImage);
        profile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), ProfileActivity.class);
                startActivity(intent);
            }
        });

        return root;
    }
}