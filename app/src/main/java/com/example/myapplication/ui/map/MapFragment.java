package com.example.myapplication.ui.map;

import android.Manifest;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.location.Location;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import com.example.myapplication.MyMarkerOptions;
import com.example.myapplication.R;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class MapFragment extends Fragment implements OnMapReadyCallback {

    private GoogleMap mMap;
    List<MyMarkerOptions> myMarkerOptionsList;
    FusedLocationProviderClient privateLocationProviderClient;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {

        View root = inflater.inflate(R.layout.fragment_map, container, false);

        myMarkerOptionsList = new ArrayList<MyMarkerOptions>();
        myMarkerOptionsList.add(new MyMarkerOptions("Торговый центр Дирижабль","Лучшая пицца на Ботанике", "pizza", 56.7961420, 60.6282740));
        myMarkerOptionsList.add(new MyMarkerOptions("Торговый центр Стрелка","Лучшая пицца на Уралмаше", "pizza", 56.888818, 60.612601));
        SupportMapFragment mapFragment = (SupportMapFragment) getChildFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
        privateLocationProviderClient = LocationServices.getFusedLocationProviderClient(requireActivity());

        return root;
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        mMap.getUiSettings().setZoomControlsEnabled(true);
        if (ActivityCompat.checkSelfPermission(requireActivity(), Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(requireActivity(), Manifest.permission.ACCESS_COARSE_LOCATION)
                        != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(requireActivity(),new String[]{
                    Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION
            },1);
        }
        if (ActivityCompat.checkSelfPermission(requireActivity(), Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(requireActivity(), Manifest.permission.ACCESS_COARSE_LOCATION)
                        == PackageManager.PERMISSION_GRANTED) {
            mMap.setMyLocationEnabled(true);
            mMap.getUiSettings().setMyLocationButtonEnabled(true);
        }
//        Task locationResult = privateLocationProviderClient.getLastLocation();
//        locationResult.addOnCompleteListener(requireActivity(), new OnCompleteListener() {
//                    @Override
//                    public void onComplete(@NonNull Task task) {
//                        if (task.isSuccessful()) {
//                            Location lastLocation = (Location) task.getResult();
//                            LatLng myLocation = new LatLng(lastLocation.getLatitude(),lastLocation.getLongitude());
//                            mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(myLocation,17));
//                        }
//                    }
//                });

        for (int i= 0;i<myMarkerOptionsList.size();i++)
        {
            mMap.addMarker(new MarkerOptions()
                    .position(new LatLng(myMarkerOptionsList.get(i).getLatitude(),myMarkerOptionsList.get(i).getLongitude()))
                    .icon(BitmapDescriptorFactory.fromBitmap(resizeBitMap("pizza",100,100)))
                    .title(myMarkerOptionsList.get(i).getTitle())
                    .snippet(myMarkerOptionsList.get(i).getSnippet()));
        }

        LatLng sydney = new LatLng(56.837805, 60.596971);
        mMap.addMarker(new MarkerOptions().position(sydney)
                .icon(BitmapDescriptorFactory.fromBitmap(resizeBitMap("pizza",100,100)))
                .title("Лучшая пицца в центре").snippet("Площадь 1905 года"));
        mMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
            @Override
            public boolean onMarkerClick(Marker marker) {
                Toast.makeText(getActivity(),"Вы нашли лучшую пиццу",Toast.LENGTH_LONG).show();
                return false;
            }
        });
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(sydney,17));
    }
    public Bitmap resizeBitMap(String drawableName, int width, int height) {
        Bitmap image = BitmapFactory.decodeResource(getResources(),getResources()
                .getIdentifier(drawableName,"drawable",getActivity().getPackageName()));
        return Bitmap.createScaledBitmap(image,width,height,false);
    }
}