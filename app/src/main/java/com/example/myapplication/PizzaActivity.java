package com.example.myapplication;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

public class PizzaActivity extends AppCompatActivity {
    TextView textView,textViewDesc;
    ImageView imageView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pizza);
        textViewDesc = findViewById(R.id.pizzaDescription);
        textView = findViewById(R.id.pizzaTitle);
        imageView = findViewById(R.id.pizzaImage);
        textViewDesc.setText(getIntent().getStringExtra("description"));
        textView.setText(getIntent().getStringExtra("title"));
        imageView.setImageResource(getIntent().getIntExtra("image",R.drawable.ic_home_black_24dp));
    }
}