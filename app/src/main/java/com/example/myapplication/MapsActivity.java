package com.example.myapplication;

import androidx.core.app.ActivityCompat;
import androidx.fragment.app.FragmentActivity;

import android.Manifest;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.widget.Toast;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.ArrayList;
import java.util.List;

public class MapsActivity extends FragmentActivity implements OnMapReadyCallback {

    private GoogleMap mMap;
    List<MyMarkerOptions> myMarkerOptionsList;
    FusedLocationProviderClient privateLocationProviderClient;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        myMarkerOptionsList = new ArrayList<MyMarkerOptions>();
        myMarkerOptionsList.add(new MyMarkerOptions("Торговый центр Дирижабль","Лучшая пицца на Ботанике", "pizza", 56.7961420, 60.6282740));
        myMarkerOptionsList.add(new MyMarkerOptions("Торговый центр Стрелка","Лучшая пицца на Уралмаше", "pizza", 56.888818, 60.612601));
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
        privateLocationProviderClient = LocationServices.getFusedLocationProviderClient(this);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        mMap.getUiSettings().setZoomControlsEnabled(true);
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION)
                        != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this,new String[]{
                    Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION
            },1);
        }
        if(ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION)
                        == PackageManager.PERMISSION_GRANTED) {
            mMap.setMyLocationEnabled(true);
            mMap.getUiSettings().setMyLocationButtonEnabled(true);
        }

        for (int i= 0;i<myMarkerOptionsList.size();i++)
        {
            mMap.addMarker(new MarkerOptions()
                    .position(new LatLng(myMarkerOptionsList.get(i).getLatitude(),myMarkerOptionsList.get(i).getLongitude()))
                    .icon(BitmapDescriptorFactory.fromBitmap(resizeBitMap("pizza",100,100)))
                    .title(myMarkerOptionsList.get(i).getTitle())
                    .snippet(myMarkerOptionsList.get(i).getSnippet()));
        }

        LatLng sydney = new LatLng(56.838026, 60.597115);
        mMap.addMarker(new MarkerOptions().position(sydney).title("Лучшая пицца в центре").snippet("Площадь 1905 года")
                .icon(BitmapDescriptorFactory.fromBitmap(resizeBitMap("pizza",100,100))));
        mMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
            @Override
            public boolean onMarkerClick(Marker marker) {
                Toast.makeText(getApplicationContext(),"Вы нашли лучшую пиццу",Toast.LENGTH_LONG).show();
                return false;
            }
        });
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(sydney,17));
    }
    public Bitmap resizeBitMap(String drawableName, int width, int height) {
        Bitmap image = BitmapFactory.decodeResource(getResources(),getResources()
                .getIdentifier(drawableName,"drawable",getPackageName()));
        return Bitmap.createScaledBitmap(image,width,height,false);
    }
}